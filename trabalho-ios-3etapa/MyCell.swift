//
//  MyCell.swift
//  trabalho-ios-3etapa
//
//  Created by COTEMIG on 09/04/1444 AH.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var actor: UILabel!
    @IBOutlet weak var imagePersonagem: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
