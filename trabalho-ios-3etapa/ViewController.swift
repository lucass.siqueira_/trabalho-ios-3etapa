//
//  ViewController.swift
//  trabalho-ios-3etapa
//
//  Created by COTEMIG on 25/03/1444 AH.
//

import UIKit
import Alamofire
import Kingfisher

struct HarryPoter: Decodable {
    let name: String
    let actor: String
    let image: String
    
}



class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDePersonagens: [HarryPoter] = []
    var lista:[HarryPoter] = []
    
    var userDefaults = UserDefaults.standard
    let listaKey = "lista"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDePersonagens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cel = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let list = listaDePersonagens[indexPath.row]
        
        cel.name.text = list.name
        cel.actor.text = list.actor
        cel.imagePersonagem?.kf.setImage(with: URL(string: list.image))
        
        return cel
    }
    
    func novopersonagem()  {
        AF.request("https://hp-api.herokuapp.com/api/characters")
            .responseDecodable(of: [HarryPoter].self) { response  in
                if let harrypoter = response.value {
                    self.listaDePersonagens = harrypoter
                }
                self.tableview.reloadData()
            }
    }
    
    
    override func viewDidLoad()	 {
        super.viewDidLoad()
        tableview.dataSource = self
        novopersonagem()
    }
    
 


    @IBOutlet weak var tableview: UITableView!
}

